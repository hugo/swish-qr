Swish QR codes
==============

Oberoende implementation av Swish's QR-koder. Byggd från avläsning av officiellt
genererade koder.

Länkar till Swishs officiella dokumentation:

- [Information om QR-koderna](https://www.swish.nu/faq/company/hur-skapar-jag-min-egen-swish-qr-kod)
- [Swishs officiella QR-web-tjänst](https://www.swish.nu/marknadsmaterial)
- [Guide Swish QR Code specification - Integration Guide v1.7.2](https://assets.ctfassets.net/zrqoyh8r449h/12uwjDy5xcCArc2ZeY5zbU/ce02e0321687bbb2aa5dbf5a50354ced/Guide-Swish-QR-code-design-specification_v1.7.2.pdf) (1)


### Kuriosa

Det verkar finnas två former av koder. De "kompakta" vilka (1) referar till
ovan (vilka bara kan tolkas effektivt av Swish-appen), samt de i URL-form (vilka
genereras här). De öppnar Swish-appen om de skannas från annat program.

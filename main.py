#!/usr/bin/env python3

from urllib.parse import urlencode
import subprocess
import re
from enum import Enum
import argparse


class PhoneNumber:
    """
    Container class for validated phone numbers.

    The constructor takes a human friendly phone number. Cleans it up
    and validates it, and then stores it in the parameter number
    (which should be thought of as read-only).
    """
    def __init__(self, number):
        self.number = PhoneNumber.__cleanup_phone_number(number)

    def __cleanup_phone_number(number: str) -> str:
        return re.sub(r'[^0-9]*', '', number)


class Lock(Enum):
    """
    Lockable fields.

    The swish app can chose to either allow or disallow editing of the
    message and money amount by the user after scanning (the number is
    always locked).
    """
    MESSAGE = 'msg'
    AMMOUNT = 'amt'


def create_swish_url(*,
                     phone: PhoneNumber,
                     price: int | None = None,
                     message: str = '',
                     unlock: set[Lock] = set()) -> str:
    """
    Generate a URL for a swish transaction.

    This URL should generally be passed along to a QR-encoding
    routine. Locking an undefined field is currently undefined
    behaviour (and will be until someone takes the time to generate
    and scan some codes).

    :param phone:
        Target phone number.
    :param price:
        Sugested/fixed price.
    :param message:
        Sugested/fixed message.
    :param unlock:
        Set of fields which should be open for editing.
    """

    args = {
        'sw': phone.number,
        'src': 'qr',
    }

    if type(price) == int:
        args['amt'] = str(price)
        args['cur'] = 'SEK'


    if message:
        args['msg'] = message

    query = urlencode(args)

    if len(unlock) > 0:
        query += '&edit=' + ','.join(ul.value for ul in unlock)

    return f"https://app.swish.nu/1/p/sw/?{query}"


def build_parser() -> argparse.ArgumentParser:
    """Construct the programs argument parser."""
    parser = argparse.ArgumentParser(
            prog="swish-qr",
            description="Generate Swish QR codes")
    parser.add_argument('-#', '--phone',
                        help="Target phone number. Will always be locked",
                        required=True,
                        action='store',
                        dest='phone')
    parser.add_argument('-$', '--price',
                        help="Target price",
                        action='store',
                        type=int,
                        dest='price')
    parser.add_argument('-m', '--msg',
                        help='Sugested message',
                        action='store',
                        dest='msg')
    parser.add_argument('-t', '--type',
                        help="Output type, same as for qrencode(1), plus 'URL'",
                        default='ansiutf8',
                        action='store',
                        dest='type')
    parser.add_argument('-o', '--output',
                        help="Output file, defaults to stdout",
                        action='store',
                        dest="output")
    parser.add_argument('-n', '--no-eol',
                        help="Don't emit a newline at end of file",
                        dest='no_eol',
                        action='store_true')
    parser.add_argument('-u', '--unlock',
                        help='Allows the end user to edit the given field',
                        choices=['message', 'm', 'ammount', 'a'],
                        action='append',
                        default=[],
                        dest='unlock')

    return parser


def main():
    """Main entry point for program."""
    args = build_parser().parse_args()

    unlocks: set[Lock] = set()
    for unlock in args.unlock:
        if unlock in {'message', 'm'}:
            unlocks.add(Lock.MESSAGE)
        elif unlock in {'ammount', 'a'}:
            unlocks.add(Lock.AMMOUNT)

    url = create_swish_url(phone=PhoneNumber(args.phone),
                           price=args.price,
                           message=args.msg,
                           unlock=unlocks)

    if args.type.lower() == 'url':
        print(url, end='' if args.no_eol else '\n')
    else:
        cmdline = ['qrencode', '-t', args.type]
        if args.output:
            cmdline += ['-o', args.output]
        subprocess.run(cmdline, input=url.encode('ASCII'))


if __name__ == '__main__':
    main()
